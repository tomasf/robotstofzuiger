# Examenpracticum Methodiek van de Informatica
# Boris Gordts & Tomas Fiers
# april - mei 2013

import random
import math
import time
from SpecificationLibrary import *

# Constants representing the robot commands
MOVE = 0
TURN = 1
CLEAN = 2
# Constants representing the robot direction
UP = 0
RIGHT = 1
DOWN = 2
LEFT = 3

#-------Model for the robot-------#
#The current position of the robot
robot_x = 0
robot_y = 0
#The direction the robot is facing
robot_direction = 0
#---------------------------------#

travel_called_times = 0;

# Represents the cells that have to be cleaned.
# It is a dictionary mapping positions to contamination values.
#   - Each key represents a position in 2D space as a tuple (x,y)
#     where x and y are integer numbers.
#     The y axis points downwards, so 'UP' for example means in the direction of lower y values.
#   - Each value represents the dirtiness of the cell 
#     as a floating point number between 0.0 and 1.0.
# e.g. {(2,3):0.1, (30,20):0.93, (45,2):0.5}
contaminated_cells = {}

# Creates a board of which all fields are clean, and with no
# robots positioned on it. This board is stored.
def make_board():
    global contaminated_cells
    contaminated_cells = {}

# Create a new robot positioned in the origin (0,0) of the current board
# and oriented upwards. This robot is stored.
def make_robot():
    global robot_x, robot_y, robot_direction
    robot_x = 0
    robot_y = 0
    robot_direction = UP


# Make some fields of the current board dirty:
#  1) if dirty_cells has a value different from None:  all cells provided by the parameter <dirty_cells> should be made dirty
#        <dirty_cells> is represented as a map of tuples with a contamination value, e.g. {(2,3):0.1,(30,20):0.93,(45,2):0.5}
#        Note that the collection of cells that will be made dirty is be a set (i.e., the same cell cannot be made dirty more than once) 
#  2) otherwise: <n> cells in the rectangle specified by <upper_left> and <lower_right> should be made dirty
#        both the positions of the dirty cells and their degree of dirtiness (between 0.1 and 1.0) are generated at random
def make_dirty(upper_left, lower_right, number_of_contaminated_cells=5, dirty_cells=None):
    global contaminated_cells
    
    if dirty_cells is not None:
        contaminated_cells.update(dirty_cells)
    else:
        for _ in range(number_of_contaminated_cells):
            position = generate_position(upper_left, lower_right)
            contamination = random.uniform(0.1, 1.0)
            contaminated_cells[position] = contamination

# Helper function for 'make_dirty'. 
# Generates a random position in the given rectangle
# and makes sure this position is not already contaminated.
# @return: A tuple of random coordinates that is not in contaminated_cells
def generate_position(upper_left, lower_right):
    #First generate a random x and y in the given rectangle
    random_x = random.randint(upper_left[0], lower_right[0])
    random_y = random.randint(upper_left[1], lower_right[1])
    position = (random_x, random_y)
    #Check whether the generated position is already contaminated.
    if position in contaminated_cells:
        #Generate a new position
        return generate_position(upper_left, lower_right)
    else:
        return position

# Return a list of all dirty areas of the current board in the rectangular area 
# delineated by the upper left corner and the lower right corner.
# The result of this method is a list of (x,y) coordinates
def get_all_dirty_areas(upper_left, lower_right):
    #Preconditions
    assert isinstance(upper_left, tuple) and isinstance(lower_right, tuple)
    assert (upper_left[0] < lower_right[0]) and (upper_left[1] < lower_right[1])
    
    dirty_cells = []
    i = 0
    while i < len(contaminated_cells.keys()):
        #Loop invariant
        assert for_all(dirty_cells, lambda position: lies_in_rectangle(upper_left, lower_right, position))
        assert for_n(contaminated_cells.keys()[:i], len(dirty_cells), lambda position: lies_in_rectangle(upper_left, lower_right, position) and position in dirty_cells)
        
        next_dirty_area = contaminated_cells.keys()[i]
        
        if lies_in_rectangle(upper_left, lower_right, next_dirty_area):
            dirty_cells += [next_dirty_area]
        
        i += 1
            
    #Postconditions
    # All positions in dirty_cells must lie in the rectangle
    assert for_all(dirty_cells, lambda position: lies_in_rectangle(upper_left, lower_right, position))
    # The positions in dirty_cells must be all the dirty areas in the given rectangle
    assert for_n(contaminated_cells.keys(), len(dirty_cells), lambda position: lies_in_rectangle(upper_left, lower_right, position) and position in dirty_cells)
    
    return dirty_cells

# Note: the correctness proof of this function is written at the end of this file.
#
# Helper function: returns True when the given position lies in the given rectangle, False otherwise
# @param rectangle_upper_left: the upper left corner of the rectangle where the position has to lie in
# @param rectangle_low_right: the lower right corner of the rectangle where the position has to lie in
# @param position: the position that must be checked
# @return True when position is in ractangle, False otherwise
def lies_in_rectangle(rectangle_upper_left, rectangle_lower_right, position):
    assert isinstance(rectangle_upper_left, tuple) and isinstance(rectangle_lower_right, tuple)
    assert (rectangle_upper_left[0] < rectangle_lower_right[0]) and (rectangle_upper_left[1] < rectangle_lower_right[1])
    
    lies_in_rectangle = False
    
    if rectangle_upper_left[0] <= position[0] <= rectangle_lower_right[0] and\
       rectangle_upper_left[1] <= position[1] <= rectangle_lower_right[1]:
            lies_in_rectangle = True
    else:
        lies_in_rectangle = False
            
    assert lies_in_rectangle == ((rectangle_upper_left[0] <= position[0] <= rectangle_lower_right[0]) and\
     (rectangle_upper_left[1] <= position[1] <= rectangle_lower_right[1]))
    
    return lies_in_rectangle

# Returns the most optimal path to run through the given set of positions.
# Each position is a tuple (x,y)
# The result is a tuple of tuples (x,y)
def get_optimal_path(start, positions):    
    # Precondition: start must be a tuple, positions a set of tuples.
    assert isinstance(start, tuple)
    assert isinstance (positions, set) and for_all(positions, lambda position: isinstance(position, tuple))
    
    # A dictionary mapping paths (ordered sequences (tuples) of (x,y) tuples) to the lengths of these paths.
    # e.g. paths = {((5,6), (1,0), (9,5)) : 26, ((5,6), (9,5), (1,0)) : 22}
    # Will be populated with all possible paths.
    paths = {}
    
    # Send out our self dividing salesman to populate 'paths'
    travel(paths, (start,), 0, positions)
    
    shortest_path = get_shortest_path(paths)
                
    # Postcondition: 'paths' must contain every possible path.
    assert len(paths) == math.factorial(len(positions))
    # Postcondition: 'shortest_path' must be the most efficient way to visit every position.
    assert for_all(paths, lambda path: paths[path] <= shortest_path);
    
    return shortest_path

# Travels recursively through all 'unvisited_positions' and appends all possible paths visiting each of those
# unvisited positions and the distances of those paths, to 'paths'.
# 
# @param 'paths' should be a reference to the dictionary 'paths' created in 'get_optimal_path'.
# @param 'path' is a tuple containing the already visited positions in order visited.
# @param 'distance_traveled' is an integer number containing the distance of the route in 'path'
# @param 'unvisited_positions' should be a set of places yet to visit.
def travel(paths, path, distance_traveled, unvisited_positions):    
    if len(unvisited_positions) == 0:
        # Trivial case: we visited every area.
        # Append the followed route and its length to the set which will contain all possible paths.
        paths[path] = distance_traveled
    else:
        # After the route already travelled, send out a salesman for each remaining city from the current city.
        for position in unvisited_positions:
            # Give this salesman updated copies of the path already travelled, its distance and the cities that remain to be visited.
            updated_path = path + (position,)
            updated_distance_traveled = distance_traveled + manhattan_distance(path[-1], position)
            updated_unvisited_positions = unvisited_positions - set((position,))
            travel(paths, updated_path, updated_distance_traveled, updated_unvisited_positions)
            
# Helper method of get_optimal_path() : returns the shortest path from 
# a given dictionary path ==> distance
def get_shortest_path(paths):
    # Find the path with the shortest length.
    shortest_path = paths.keys()[0]
    for path in paths:
        if paths[path] < paths[shortest_path]:
            shortest_path = path
    
    return shortest_path

#Timings
def get_optimal_path_timings():
    for i in range(1,11):
        print i, " different positions"
        positions = set()
        while len(positions) < i:
            random_x = random.randrange(20)
            random_y = random.randrange(20)
            
            next_position_tuple = (random_x, random_y)
            
            #When positions already contains next_position_tuple, nothing is added
            positions.add(next_position_tuple)
        
        before = time.clock();
        get_optimal_path(list(positions)[0], positions)
        print "Get optimal path: ", (time.clock() - before)
        
        paths = {}
        
        #Timings for travel
        before = time.clock();
        travel(paths, (list(positions)[0],), 0, positions)
        print "Travel: ", (time.clock() - before)
        
        #Timings for get_shortest_path
        before = time.clock();
        get_shortest_path(paths)
        print "Get_shortest_path: ", (time.clock() - before)

# Returns the manhattan distance between points p1 = (x1, y1) and p2 = (x2, y2).
# The manhattan distance or taxicab distance is the shortest distance between two points
# if you can only travel parallel to the coordinate axes.
# This shortest distance can be travelled in multiple ways/
# e.g. RIGHT, RIGHT, RIGHT, UP, UP is equivalent to RIGHT, UP, RIGHT, RIGHT, UP
def manhattan_distance(p1, p2):
    return abs(p1[0] - p2[0]) + abs(p1[1] - p2[1])

# Return a list of commands necessary to clean the board given an ordered tuple of (x,y) tuples.
# Assumes the first position is the position the robot is standing on.
# Result is a list of MOVE, TURN and CLEAN commands
# e.g., (MOVE,TURN,TURN,TURN,MOVE,MOVE,CLEAN,MOVE,MOVE,CLEAN)
def get_commands_to_clean(positions):
    commands = []
    dirr = get_robot_direction()
    i = 1
    while i < len(positions):
        new_commands, dirr = get_travelling_commands_between(positions[i-1], positions[i], dirr)
        commands += new_commands + [CLEAN]
        i += 1
    return commands

# Helper function for 'get_commands_to_clean'.
# Returns a list of commands and the new direction the robot is facing.
# The list of commands move the robot from p1 = (x1,y1) to p2 = (x2,y2) using manhattan or taxicab travelling
# assuming the robot is starting facing 'direction'.
# The coordinates should be integer numbers.
def get_travelling_commands_between(p1, p2, direction):
    x1, y1, x2, y2 = p1[0], p1[1], p2[0], p2[1]
    commands = []

    # First bridge the horizontal distance...
    if x1 != x2:
        # Start by turning to the right direction.
        while direction != (RIGHT if x1 < x2 else LEFT):
            direction = (direction + 1) % 4
            commands += [TURN]
        # Keep moving one tile right or left until the horizontal distance is bridged.
        x = x1
        while x != x2:
            x += 1 if x1 < x2 else -1
            commands += [MOVE]

    # ...then bridge the remaining vertical distance.
    if y1 != y2:
        # Start by turning to the right direction again.
        while direction != (DOWN if y1 < y2 else UP):
            direction = (direction + 1) % 4
            commands += [TURN]
        # Keep moving one tile down or up until the vertical distance is bridged.
        y = y1
        while y != y2:
            y += 1 if y1 < y2 else -1
            commands += [MOVE]

    return commands, direction

# Clean the cell where the robot is currently situated by 0.4 DP (dirtiness points)
def make_clean():
    # Only contaminated cells have to be cleaned.
    if (robot_x, robot_y) in contaminated_cells:
        contamination = contaminated_cells[(robot_x, robot_y)]
        if contamination > 0.4:
            contaminated_cells[(robot_x, robot_y)] = contamination - 0.4
        else:
            del contaminated_cells[(robot_x, robot_y)]

# Move the given robot one step in the direction in which it is currently facing.
def robot_move():
    global robot_x, robot_y
    dx, dy = {UP: (0,-1), DOWN: (0,1), RIGHT: (1,0), LEFT: (-1,0)}[robot_direction]
    robot_x += dx
    robot_y += dy

# Turn the robot clockwise over 90 degrees.
def robot_turn():
    global robot_direction
    robot_direction = (robot_direction + 1) % 4

# Get the current direction of the robot.
def get_robot_direction():
    return robot_direction

# Get the current x-coordinate of the position of the robot.
def get_robot_x():
    return robot_x

# Get the current y-coordinate of the position of the robot.
def get_robot_y():
    return robot_y

# Get the contamination of the cell at the given position on the current board 
# the result is an integer between 0.0 and 1.0 (0.0 means clean cell)
def get_cell_contamination(position):
    return contaminated_cells.get(position, 0) # Geeft 0 als 'position' niet gevonden wordt.

# Time complexity of 'get_optimal_path()'
#
# The core of our get_opimal_path algorithm is the recursive travel function. This function 
# "travels recursively through all 'unvisited_positions' and appends all possible paths visiting each of those
# unvisited positions and the distances of those paths, to 'paths'". This function will consume most of the
# needed execution time, so we will investigate how many times this function gets called.
# 
# n = 1:  When the amount of positions equals 1, travel gets called 2 times: one time from get_optimal_path() 
#         and one time recursively
# n = 2:  Now we have two times the first case (n = 1) + one from get_optimal_path() ==> 2(2)+1 = 5
# n = 3:  Two times the previous case + 1 from get_optimal_path() ==> 3(5) + 1 = 16
# n    :  n * (result for n - 1) + 1
#
# In general: the amount of function calls seems to be scaling like n!. We presume that
# the general form of the function will be #number_of_calls = a*(n!). We calculate a by comparing the number
# of calls our function will have for n=8 and 8!.
# 8! = 40,320 and n * (result for n - 1) + 1 = 109,601. a = 109.601/40.320 = 2.71827877. So the number of
# calls for n positions is proximately 2.7183 * (n!).
#
# The get_shortest_path helperfunction is also
# worth some investigation. This function loops over all possible paths and returns the shortest one. To do
# this, it has to loop over the whole dictionary. In the Python documentation, this operation has a time-complexity
# of O(n), with n the number of key->value pairs in the dictionary. So in our code, this is O(n!), because there are
# n! possible paths between n positions. However, these calls will not need as much time as the recursive function does.
#
# So this algorithm has O(n! + n!) = O(2n!) = O(n!) function calls. The travel function calls will be more time-
# consuming than the calls in get_shortest_path. We make the approximation that every call will take the same amount of time.
# To get the time needed for n positions, we divide the time needed for a small n that we can measure by this small n, and
# multiply it by the n for which we want to know the needed execution time.
# So the time-complexity of our algorithm has the general form a*n^b, where n^b = n!. The a parameter is system specific.
# For one of our laptops (Core i7 3610 QM processor on Windows 8 x64), get_optimal_path for 10 positions takes 21.3571254861 seconds. 
# 21.3571254861 / 10! = 5.885451247271825e-06 = a. So in general, this algorithm will take for n positions approximately
# 5.885451247271825e-06 * n! seconds. A quick check:
# n        Timed (averages used)     Calculated
#
# 4        0.0001191429738           0.00014125082993452382
# 5        0.000640783933994         0.000706254149672619
# 6        0.00431324338857          0.004237524898035714
# 7        0.0228576018349           0.02966267428625



# Correctness loop in get_all_dirty_areas
#
#  Prologue
#    Given: - assert isinstance(upper_left, tuple) and isinstance(lower_right, tuple)
#           - assert (upper_left[0] < lower_right[0]) and (upper_left[1] < lower_right[1])
#
#    for_all([], lambda position: lies_in_rectangle(upper_left, lower_right, position))
#    for_n([], len(dirty_cells), lambda position: lies_in_rectangle(upper_left, lower_right, position) and position in [])
#    
#    dirty_cells = []
#    i = 0
#
#    for_all(dirty_cells, lambda position: lies_in_rectangle(upper_left, lower_right, position))
#    for_n(contaminated_cells.keys()[:i], len(dirty_cells), lambda position: lies_in_rectangle(upper_left, lower_right, position) and position in dirty_cells)
#
#  Body
#    Given: - i < len(contaminated_cells.keys())
#           - for_all(dirty_cells, lambda position: lies_in_rectangle(upper_left, lower_right, position))
#           - for_n(contaminated_cells.keys()[:i], len(dirty_cells), lambda position: lies_in_rectangle(upper_left, lower_right, position) and position in dirty_cells)
#
#        #One of the two parts of the asserts is always True.
#        assert for_all(dirty_cells + [contaminated_cells.keys()[i]], lambda position: lies_in_rectangle(upper_left, lower_right, position)) or\
#               for_all(dirty_cells, lambda position: lies_in_rectangle(upper_left, lower_right, position))
#        assert for_n(contaminated_cells.keys()[:(i + 1)], len(dirty_cells + [contaminated_cells.keys()[i]]), lambda position: lies_in_rectangle(upper_left, lower_right, position) and position in dirty_cells) or\
#               for_n(contaminated_cells.keys()[:(i + 1)], len(dirty_cells), lambda position: lies_in_rectangle(upper_left, lower_right, position) and position in dirty_cells)
#
#        next_dirty_area = contaminated_cells.keys()[i]
#        
#        if lies_in_rectangle(upper_left, lower_right, next_dirty_area):
#            assert for_all(dirty_cells + [next_dirty_area], lambda position: lies_in_rectangle(upper_left, lower_right, position))
#            assert for_n(contaminated_cells.keys()[:(i + 1)], len(dirty_cells + [next_dirty_area]), lambda position: lies_in_rectangle(upper_left, lower_right, position) and position in dirty_cells)
#
#            dirty_cells += [next_dirty_area]
#
#        assert for_all(dirty_cells, lambda position: lies_in_rectangle(upper_left, lower_right, position))
#        assert for_n(contaminated_cells.keys()[:(i + 1)], len(dirty_cells), lambda position: lies_in_rectangle(upper_left, lower_right, position) and position in dirty_cells)
#        
#        i += 1
#
#        assert for_all(dirty_cells, lambda position: lies_in_rectangle(upper_left, lower_right, position))
#        assert for_n(contaminated_cells.keys()[:i], len(dirty_cells), lambda position: lies_in_rectangle(upper_left, lower_right, position) and position in dirty_cells)
#
#  Epilogue
#    Given: - assert i >= len(contaminated_cells.keys())
#           - assert for_all(dirty_cells, lambda position: lies_in_rectangle(upper_left, lower_right, position))
#           - assert for_n(contaminated_cells.keys()[:i], len(dirty_cells), lambda position: lies_in_rectangle(upper_left, lower_right, position) and position in dirty_cells)
#    # First postcondition is given
#    assert for_all(dirty_cells, lambda position: lies_in_rectangle(upper_left, lower_right, position))
#    
#    # i is equal or larger than the length of contaminated_cells.keys(), so this follows from the second loop invariant
#    assert for_n(contaminated_cells.keys(), len(dirty_cells), lambda position: lies_in_rectangle(upper_left, lower_right, position) and position in dirty_cells)
#
#
# Correctness lies_in_rectangle
#
# Preconditions
# assert isinstance(rectangle_upper_left, tuple) and isinstance(rectangle_lower_right, tuple)
# assert (rectangle_upper_left[0] < rectangle_lower_right[0]) and (rectangle_upper_left[1] < rectangle_lower_right[1])
#    
#    # This statement is not part of our backward substitution, because it will get a new value assigned for sure (because of the if AND else statement). 
#    # lies_in_rectangle = False
#
#    #This assert will pass either way, because the only two possibilities are both correct
#    assert True == ((rectangle_upper_left[0] <= position[0] <= rectangle_lower_right[0]) and\
#                 (rectangle_upper_left[1] <= position[1] <= rectangle_lower_right[1])) or\
#           False == ((rectangle_upper_left[0] <= position[0] <= rectangle_lower_right[0]) and\
#                 (rectangle_upper_left[1] <= position[1] <= rectangle_lower_right[1]))
#
#    if rectangle_upper_left[0] <= position[0] <= rectangle_lower_right[0] and\
#       rectangle_upper_left[1] <= position[1] <= rectangle_lower_right[1]:
#            assert True == ((rectangle_upper_left[0] <= position[0] <= rectangle_lower_right[0]) and\
#                 (rectangle_upper_left[1] <= position[1] <= rectangle_lower_right[1]))
#            
#            lies_in_rectangle = True
#    else:
#        assert False == ((rectangle_upper_left[0] <= position[0] <= rectangle_lower_right[0]) and\
#             (rectangle_upper_left[1] <= position[1] <= rectangle_lower_right[1]))
#
#        lies_in_rectangle = False
#
# Postconditions
# assert lies_in_rectangle == ((rectangle_upper_left[0] <= position[0] <= rectangle_lower_right[0]) and\
#     (rectangle_upper_left[1] <= position[1] <= rectangle_lower_right[1]))
