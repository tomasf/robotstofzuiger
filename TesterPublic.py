from Deel1Asked import *
from SpecificationLibrary import *
UP = 0
RIGHT = 1
DOWN = 2
LEFT = 3
MOVE = 0
TURN = 1
CLEAN = 2

def test_make_board():
    make_board()
    assert len(get_all_dirty_areas((0,0),(10,10)))==0
    
def test_make_robot():
    make_robot()
    assert get_robot_direction()==UP
    assert get_robot_x()==0
    assert get_robot_y()==0

# test make_dirty and get_all_dirty_areas
def test_make_dirty():
    make_board()
    make_robot()
    make_dirty((0,0),(20,20),10)
    # there should be exactly 10 dirty cells
    assert len(get_all_dirty_areas((0,0),(20,20)))==10
    # there should be maximum 10 dirty cells
    assert len(get_all_dirty_areas((10,10),(20,20)))<=10

# test get optimal path
def test_get_optimal_path():
    make_board()
    make_robot()
    cells = {(2,3):0.1,(20,20):0.5,(10,10):1.0,(0,0):0.75}
    make_dirty((0,0),(20,20),dirty_cells=cells)
    path = get_optimal_path((get_robot_x(),get_robot_y()), set(cells))
    assert len(path)==5

# test get commands to clean
def test_get_commands_to_clean():
    make_board()
    make_robot()
    cells = {(2,3):0.1,(20,20):0.5,(10,10):1.0,(0,0):0.75}
    make_dirty((0,0),(20,20),dirty_cells=cells)
    path = get_optimal_path((get_robot_x(),get_robot_y()),set(cells))
    commands = get_commands_to_clean(path)
    assert len(commands) <= 60
    assert for_some(commands, lambda command: command==MOVE)
    assert for_some(commands, lambda command: command==CLEAN)
    assert for_some(commands, lambda command: command==TURN)

print "running tests"
try:
    test_make_board()
except (AssertionError,TypeError):
    print "ERROR make empty board failed"
try:
    test_make_robot()
except (AssertionError,TypeError):
    print "ERROR make robot failed"

try:
    test_make_dirty()
except (AssertionError,TypeError):
    print "ERROR make_dirty with predefined dirty cells / get_all dirty_areas"

try:
    test_get_optimal_path()
except (AssertionError,TypeError):
    print "ERROR get_optimal_path"

try:
    test_get_commands_to_clean()
except (AssertionError,TypeError):
    print "ERROR get_commands_to_clean"
print "done!"
