from Tkinter import *
import tkMessageBox
from Deel1Asked import *

BOARD_WIDTH = 23
BOARD_HEIGHT = 23
CELL_SIZE = 20
OFFSET = 20
UP = 0
RIGHT = 1
DOWN = 2
LEFT = 3
MOVE=0
TURN=1
CLEAN=2
UPPER_LEFT = (0,0)
LOWER_RIGHT = (BOARD_WIDTH-1,BOARD_HEIGHT-1)
squares = [[]]
robot_gif = None
robot_image = None
canvas = None
cleaning_sequence = None
cleaning_counter = 0
    
# create the graphical user interface
def draw_board(root):
    global squares, canvas
    root.title("Robot UI")
    canvas = Canvas(root, bg="white", height=520, width=650)
    for i in range(BOARD_WIDTH):
        squares.append([])
        for j in range(BOARD_HEIGHT):
            squares[i].append(canvas.create_rectangle(OFFSET+j*CELL_SIZE, OFFSET+i*CELL_SIZE, OFFSET+(j+1)*CELL_SIZE, OFFSET+(i+1)*CELL_SIZE, fill="white"))
    # Draw the make_dirty button
    canvas.create_window(500, 20, anchor = NW, window=Button(canvas, text="Make dirty", command=make_board_dirty, width=10, relief=FLAT))
    # Draw the make_dirty button
    canvas.create_window(500, 50, anchor = NW, window=Button(canvas, text="Clean", command=clean, width=5, relief=FLAT))
    # Draw the quit button
    canvas.create_window(500, 80, anchor = NW, window=Button(canvas, text="Quit", command=root.destroy, width=5, relief=FLAT))
    # draw the canvas
    canvas.pack(expand=YES,fill=BOTH)
    # Draw the robot
    draw_robot()

# make dirty callback
def make_board_dirty():
    # make dirty random
    make_dirty(UPPER_LEFT,LOWER_RIGHT,5)

    # make dirty manual
    #cells = {(2,3):.1,(20,20):.5,(10,10):1.0}
#    cells = {(0,0):.4,(12,1):.4,(11,12):.4,(2,14):.4,(2,15):.4,(18,18):.4}
#   make_dirty(0,0,dirty_cells=cells)
    refresh_canvas()
    
# clean callback
def clean():
    global cleaning_sequence,cleaning_counter
    dirty_cells = get_all_dirty_areas(UPPER_LEFT,LOWER_RIGHT)
    optimal_path = get_optimal_path((get_robot_x(),get_robot_y()),set(dirty_cells))
    if len(optimal_path)>1:
        cleaning_sequence = get_commands_to_clean(optimal_path)
        cleaning_counter = 0
        execute_command()
    else:
        tkMessageBox.showerror("Board is clean", "The board is clean nothing to do!")

def execute_command():
    global cleaning_sequence, cleaning_counter
    timeout = 100
    if cleaning_sequence[cleaning_counter]==MOVE:
        make_robot_move()
    elif cleaning_sequence[cleaning_counter]==TURN:
        make_robot_turn()
    else:
        make_cell_clean()
        timeout = 500
    if cleaning_counter<len(cleaning_sequence)-1:
        cleaning_counter+=1
        root.after(timeout, lambda pos=cleaning_sequence[cleaning_counter]: execute_command())
    else:
        root.after(timeout, lambda: draw_robot())

def make_cell_clean():
    global canvas
    canvas.delete(robot_image)
    draw_robot("cleaning.gif")
    make_clean()
    current_position = (get_robot_x(),get_robot_y())
    canvas.itemconfig(squares[get_robot_y()][get_robot_x()], fill=get_shade(get_cell_contamination(current_position)))
    
def make_robot_move():
    draw_robot()
    robot_move()
    canvas.coords(robot_image,OFFSET/2+CELL_SIZE+CELL_SIZE*get_robot_x(), OFFSET/2+CELL_SIZE+CELL_SIZE*get_robot_y())
    
def make_robot_turn():
    canvas.delete(robot_image)
    robot_turn()
    draw_robot()

def draw_robot(bitmap=None):
    global robot_gif, robot_image
    if bitmap==None:
        robot_gif = PhotoImage(file="robot"+str(get_robot_direction())+".gif")
    else:
        robot_gif = PhotoImage(file=bitmap)
    robot_image = canvas.create_image(OFFSET/2+CELL_SIZE+CELL_SIZE*get_robot_x(),OFFSET/2+CELL_SIZE+CELL_SIZE*get_robot_y(),image=robot_gif, anchor=CENTER)
    
def clear_canvas():
    global canvas, squares
    for i in range(BOARD_WIDTH):
        for j in range(BOARD_HEIGHT):
            canvas.itemconfig(squares[i][j], fill="white")
            
def refresh_canvas():
    clear_canvas()
    positions = get_all_dirty_areas(UPPER_LEFT,LOWER_RIGHT)
    for pos in positions:
        # the squares are reversed in comparison with X and Y coordinates
        canvas.itemconfig(squares[pos[1]][pos[0]], fill=get_shade(get_cell_contamination(pos)))

# return the color shade for a given value. 0.0 = white, 1.0 = black
def get_shade(value):
    if value==0:
        return "white"
    elif 0<value<.25:
        return "#C3C3C3"
    elif .25<=value<.50:
        return "#7E7E7E"
    elif .50<=value<.75:
        return "#3E3E3E"
    else:
        return "black"

make_board()
make_robot()
root = Tk()
draw_board(root)
root.mainloop()